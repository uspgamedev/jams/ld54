extends Node
class_name Navigator

@export_flags("Land", "Water", "Air") var movement_types = 0
@export var debug := false

@onready var _entity := Entity.get_entity(self)
@onready var _world := Entity.get_world(self)

var count = 0

func _physics_process(_delta):
	if count <= 4:
		count += 1
		return
	count = 0
	if not _entity.tile:
		return
	var mask_weights := {}
	var tiles := _world.get_neighbors(_entity.tile)
	var tactical_level := {}
	_entity.propagate_call("_navigate", [mask_weights])
	tiles.append(_entity.tile)
	tiles = tiles.filter(func(tile: Tile): return tile.movement & movement_types)
	if tiles.is_empty():
		_entity.target_tile = null
		return
	for tile in tiles:
		var level = 0
		for mask in mask_weights:
			level += tile.get_tactical_level(mask, movement_types) * mask_weights[mask]
		tactical_level[tile] = level
	var target_tile: Tile = tiles.reduce(
		func(tile1: Tile, tile2: Tile):
			var level1 := tactical_level[tile1] as float
			var level2 := tactical_level[tile2] as float
			return tile1 if level1 < level2 else tile2
	)
	_entity.target_tile = target_tile
	if debug:
		_entity.debug.show()
		var text = "%s\n%s\n%s" % [
			var_to_str(
				tiles.map(
					func(t): return tactical_level[t]
				)
			),
			var_to_str(
				tiles.map(
					func(t): return t.get_tactical_level(PreyEvaluator.get_mask_name(0), movement_types)
				)
			),
			var_to_str(
				tiles.map(
					func(t): return t.get_tactical_level(PreyEvaluator.get_mask_name(1), movement_types)
				)
			)
		]
		_entity.debug.text = text
	else:
		_entity.debug.hide()
