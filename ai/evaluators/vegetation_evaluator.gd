class_name VegetationEvaluator
extends Evaluator

const MASK := &"vegetation"

@export var weight := 1.0

func _evaluate(tile: Tile) -> float:
	return -tile.vegetation * weight
