class_name Evaluator
extends Resource


func get_mask() -> StringName:
	return get_script().MASK


func evaluate(tile: Tile) -> float:
	return _evaluate(tile)


func _evaluate(_tile: Tile) -> float:
	return INF


static func _tactical_mask(mask: StringName, movement_profile: TileType.Movement) -> String:
	return "%s:%d" % [mask, movement_profile]
