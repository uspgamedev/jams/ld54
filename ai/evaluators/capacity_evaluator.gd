class_name CapacityEvaluator
extends Evaluator

const MASK := &"capacity"

func _evaluate(tile: Tile) -> float:
	var sum := 0
	for entity in tile.entities:
		var size: Size = entity.get_component(Size)
		sum += size.size if size else 0
	return -10 if sum > tile.capacity else 0
