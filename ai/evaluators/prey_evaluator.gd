class_name PreyEvaluator
extends Evaluator

@export var prey_level := 0

func get_mask() -> StringName:
	return PreyEvaluator.get_mask_name(prey_level)
	
static func get_mask_name(prey_level_) -> StringName:
	return StringName("prey_%d" % prey_level_)


func _evaluate(tile: Tile) -> float:
	var level_sum := 0
	
	for entity in tile.entities:
		var creature: Creature = entity.get_component(Creature)
		if creature and creature.prey_level == prey_level:
				level_sum += 1
	
	
	return -level_sum * 10
