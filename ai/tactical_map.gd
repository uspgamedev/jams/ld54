class_name TacticalMap
extends Node

@export var world: World
@export var tiles: Node3D
@export var evaluators: Array[Evaluator] = []


func _ready():
	_run()


func _run():
	while true:
		for evaluator in evaluators:
			for movement_profile in range(1, 8):
				var map := {}
				var queue: Array[Tile] = []
				# Set initial levels
				for tile in tiles.get_children():
					if tile is Tile:
						if true: # or _valid(tile, movement_profile):
							map[tile] = evaluator.evaluate(tile) + world.get_neighbors(tile).map(func(tile): return map[tile] if map.has(tile) else evaluator.evaluate(tile)).reduce(
								func(level, acc):
									return acc + 0.5 * level
							)
						else:
							map[tile] = INF
						if false:# not is_inf(map[tile]):
							queue.append(tile)
				queue.sort_custom(func(tile1, tile2): return map[tile1] < map[tile2])
				# Spread levels
				while not queue.is_empty():
					var tile := queue.pop_front() as Tile
					var level: float = map[tile] + 1.0
					var neighbors := world.get_neighbors(tile)
					for next_tile in neighbors:
						if _valid(next_tile, movement_profile) and level < map[next_tile]:
							map[next_tile] = level
							_insert(queue, next_tile, map)
				# Update tile states
				for tile in tiles.get_children():
					if tile is Tile:
						tile.set_tactical_level(
							evaluator.get_mask(), movement_profile, map[tile]
						)
				await get_tree().create_timer(0.2).timeout


func _insert(queue: Array[Tile], tile: Tile, map: Dictionary):
	if tile in queue:
		queue.erase(tile)
	queue.append(tile)
	var n := queue.size()
	for i in n - 1:
		var tile1 := queue[n - i - 1]
		var tile2 := queue[n - i - 2]
		if map[tile1] < map[tile2]:
			queue[n - i - 1] = tile2
			queue[n - i - 2] = tile1
		else:
			break


func _valid(tile: Tile, movement: TileType.Movement) -> bool:
	return (
		tile.movement & movement and tile.entities.size() < tile.capacity
	)
