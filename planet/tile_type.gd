class_name TileType
extends Resource

enum Movement {
	LAND = 1,
	WATER = 2,
	AIR = 4
}

@export var tile_name: String
@export var texture: Texture2D
@export var props: Array[PackedScene]
@export var props_num := 5
@export var movement: Movement = Movement.LAND
@export var vegetation_max := 0.0
@export var vegetation_growth_rate := 0.0
@export var capacity := 5


func set_all(tile: Tile):
	tile.movement = movement
	tile.vegetation = vegetation_max
	tile.capacity = capacity
