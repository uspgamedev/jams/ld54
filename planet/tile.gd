@tool
class_name Tile
extends MeshInstance3D

@export var tile_type: TileType: set = set_tile_type
@export_enum("0", "1", "2", "3", "4", "5") var orientation := 0 : set = set_orientation 
@export var flip := 0 : set = set_flip 


@export var collision_shape: Shape3D

var movement: int = TileType.Movement.LAND
var vegetation: float = 0.0
var capacity := 5

@onready var collision: CollisionShape3D = $Area3D/CollisionShape3D

@onready var _tactical_levels := {}
var entities : Array[Entity] = []


func set_tile_type(new_type: TileType):
	tile_type = new_type
	if new_type:
		material_override = StandardMaterial3D.new()
		material_override.albedo_texture = new_type.texture

	

func _ready():
	collision.shape = collision_shape
	if not Engine.is_editor_hint() and tile_type:
		tile_type.set_all(self)
		if tile_type.props.is_empty():
			return
		for _i in tile_type.props_num:
			var prop := tile_type.props.pick_random().instantiate() as Entity
			var offset := Vector2(randf(), randf())
			var pos := get_center_with_offset(offset)
			var coords := Entity.position_to_coord(pos)
			prop.longitude = coords.x
			prop.latitude = coords.y
			prop.add_to_world(get_tree(), false, true)
			await get_tree().create_timer(0.2).timeout


func _physics_process(delta):
	if not Engine.is_editor_hint():
		vegetation = min(
			tile_type.vegetation_max,
			vegetation + tile_type.vegetation_growth_rate * delta
		)


func _process(_delta):
	if not Engine.is_editor_hint():
		var center := get_center() * 1.1
		$Label3D.position = center
		$Label3D.text = "%.1f" % (
			get_tactical_level(PreyEvaluator.get_mask_name(0), TileType.Movement.WATER)
		)
#		$Label3D.text += " (%d/%d)" % [entities.size(), capacity]
#		$Label3D.text = "%.2f" % vegetation


func get_tactical_level(
	mask: StringName, movement_profile: TileType.Movement
) -> float:
	return _tactical_levels.get(Evaluator._tactical_mask(mask, movement_profile), 0.0)


func set_tactical_level(
	mask: StringName, movement_profile: TileType.Movement, level: float
):
	_tactical_levels[Evaluator._tactical_mask(mask, movement_profile)] = level


func _on_area_3d_area_entered(area):
	if Entity.get_entity(area) is Entity:
		var entity = Entity.get_entity(area)
		entity.tile = self
		add_entitiy(entity)
		
func _on_area_3d_area_exited(area):
	if Entity.get_entity(area) is Entity:
		var entity = Entity.get_entity(area)
		remove_entity(entity)

func _on_area_3d_mouse_entered():
	material_override.albedo_color = Color.GRAY
	var petshop = get_tree().get_first_node_in_group("PETSHOP")
	petshop.current_tile = self
#	$Label3D.visible = true

func _on_area_3d_mouse_exited():
	material_override.albedo_color = Color.WHITE
	var petshop = get_tree().get_first_node_in_group("PETSHOP")
	petshop.current_tile = null
#	$Label3D.visible = false
	
func get_center() -> Vector3:
	var center = Vector3()
	var points = self.mesh.get_faces()
	
	for point in points:
		center += point
		
	center /= points.size()
	
	return center

func get_center_with_offset(offset: Vector2) -> Vector3:
	var points = self.mesh.get_faces()
	var origin: Vector3 = points[0]
	var side1: Vector3 = points[1] - points[0]
	var side2: Vector3 = points[2] - points[0]
	var opposite := origin + side1 + side2
	var candidate1 := origin + offset.x * side1 + offset.y * side2
	var candidate2 := opposite - offset.x * side1 - offset.y * side2
	if candidate1.distance_squared_to(origin) < candidate2.distance_squared_to(origin):
		return candidate1
	return candidate2

func get_entities() -> Array[Entity]:
	return entities
	
func add_entitiy(entity: Entity):
	entities.push_back(entity)
	
func remove_entity(entity: Entity):
	entities = entities.filter(func(e): return e != entity)

func get_texture() -> Texture2D:
	return tile_type.texture
	

func set_orientation(_orientation):
	orientation = _orientation
	
	var vertices = mesh.get_faces()
	var uv = Tile.get_uvs()[orientation]

	var aux = uv[(flip+1)%3]
	uv[(flip+1)%3] = uv[flip%3]
	uv[flip%3] = aux

	var new_mesh = ArrayMesh.new()
	var arrays = []
	arrays.resize(Mesh.ARRAY_MAX)
	arrays[Mesh.ARRAY_VERTEX] = vertices
	arrays[Mesh.ARRAY_TEX_UV] = uv

	new_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
	
	mesh = new_mesh

func set_flip(_flip):
	flip = _flip
	set_orientation(orientation)

func _on_area_3d_input_event(_camera, event, _position, _normal, _shape_idx):
	if event is InputEventMouseButton and event.is_pressed() and \
			 event.button_index == MOUSE_BUTTON_LEFT:
		get_tree().get_first_node_in_group("INSPECTOR").show_tile(self)

static func get_uvs() -> Array[PackedVector2Array]:
	return [
	PackedVector2Array([
		Vector2(0, 0),
		Vector2(1./3., -1.),
		Vector2(2./3., 0),
	]),
	PackedVector2Array([
		Vector2(1./3., 0),
		Vector2(2./3., 1.),
		Vector2(1., 0),
	]),
	PackedVector2Array([
		Vector2(2./3., 1.),
		Vector2(1., 0),
		Vector2(1./3., 0),
	]),
	PackedVector2Array([
		Vector2(1./3., 0),
		Vector2(1., 0),
		Vector2(2./3., 1.),
	]),
	PackedVector2Array([
		Vector2(0, 0),
		Vector2(2./3., 0),
		Vector2(1./3., -1.),
	]),
	PackedVector2Array([
		Vector2(1./3., -1.),
		Vector2(2./3., 0),
		Vector2(0, 0),
	]),
]
