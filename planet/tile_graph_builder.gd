class_name TileGraphBuilder
extends RefCounted

var _tiles_for_edge := {}
var _tiles: Array[NodePath] = []
var _world: World


func _init(world: World):
	_world = world


func add_tile(tile: Tile, vertices: PackedVector3Array):
	_tiles.append(_world.get_path_to(tile))
	var n := vertices.size()
	for i in n:
		var vtx1 := vertices[i]
		var vtx2 := vertices[(i + 1) % n]
		var edge := _fmt_edge(vtx1, vtx2)
		var tiles := _tiles_for_edge.get(edge, []) as Array
		tiles.append(_world.get_path_to(tile))
		_tiles_for_edge[edge] = tiles


func dump():
	print("dumping %d" % _tiles_for_edge.size())
	for edge in _tiles_for_edge:
		var tiles = _tiles_for_edge[edge]
		var line: String = edge + " -> "
		for tile in tiles:
			var tile_name = tile.get_name(tile.get_name_count() - 1)
			line += tile_name + ", "
		print(line)


func build() -> TileGraph:
	var graph := TileGraph.new(_tiles)
	for edge in _tiles_for_edge:
		var tiles := _tiles_for_edge[edge] as Array
		if tiles.size() == 2:
			graph.link(tiles[0], tiles[1])
	return graph


func _fmt_edge(vertex1: Vector3, vertex2: Vector3) -> String:
	if vertex1 < vertex2:
		return _fmt_vertex(vertex1) + ":" + _fmt_vertex(vertex2)
	return _fmt_vertex(vertex2) + ":" + _fmt_vertex(vertex1)


func _fmt_vertex(vertex: Vector3) -> String:
	return "(%.4f,%.4f,%.4f)" % [vertex.x, vertex.y, vertex.z]
