class_name TileGraph
extends Resource

@export var neighbors := {}


func _init(tiles: Array[NodePath] = []):
	for tile in tiles:
		neighbors[tile] = []


func link(tile1: NodePath, tile2: NodePath):
	neighbors[tile1].append(tile2)
	neighbors[tile2].append(tile1)
