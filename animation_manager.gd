extends Sprite3D
class_name Body

@onready var animator := $Animator
@onready var entity := Entity.get_entity(self)

@export var shiny_texture: Texture2D

func _ready():
	if shiny_texture and randi_range(0, 100) < 10:
		self.texture = shiny_texture
		
func _process(_delta):
	if entity and entity.moving:
		animator.speed_scale = 1.6
		animator.play("walking")
	else:
		animator.play("RESET")
		animator.speed_scale = 1


func _on_area_3d_input_event(_camera, event, _position, _normal, _shape_idx):
	if event is InputEventMouseButton and event.is_pressed() and \
			 event.button_index == MOUSE_BUTTON_LEFT:
		get_tree().get_first_node_in_group("INSPECTOR").show_entity(Entity.get_entity(self))


func _on_area_3d_mouse_entered():
	modulate = Color.GRAY


func _on_area_3d_mouse_exited():
	modulate = Color.WHITE
