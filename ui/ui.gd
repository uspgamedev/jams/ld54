extends Control

signal stage_reset


func _on_reset_button_pressed():
	stage_reset.emit()
