extends HBoxContainer


@export var texture : Texture2D : set = set_texture

@export var entity_name := ""
@export var value := 0 : set = set_value
@export var max_value := 0 : set = set_max_value

func set_texture(texture_):
	texture = texture_
	%Icon.texture = texture

func set_value(value_):
	value = value_
	%Value.text = "%d/%d" % [value, max_value]

func set_max_value(max_value_):
	max_value = max_value_
	%Value.text = "%d/%d" % [value, max_value]
