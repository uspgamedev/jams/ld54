extends Label


func _process(_delta):	
	var stage: Stage = get_tree().get_first_node_in_group("STAGE")
	
	if stage:
		var seconds = int(stage.time_to_lose - stage.lose_condition.get_total_elapsed_time())
		
		
		text = "%02d:%02d" % [seconds/60, seconds%60]
