@tool
extends Control

@export var texture : Texture : set = set_texture
@export var bar_texture : Texture : set = set_bar_texture
@export var value:= 100.0
@export var color := Color.WHITE : set = set_color

func update_bar(value_: int, max_value: int):
	self.visible = true
	value = int(100 * value_ / float(max_value))
	%StatsBar.value = value
#	%StatsBar. = value
	%Max.text = str(max_value)
	var width = %StatsIcon.anchor_right - %StatsIcon.anchor_left
	var pos = value_ / float(max_value)
	
	if pos + width > 1.0:
		pos = 1.0
	
	%StatsIcon.anchor_left = pos
	%StatsIcon.anchor_right = %StatsIcon.anchor_left + width

func set_texture(texture_):
	texture = texture_
	%StatsIcon.texture = texture 
	
func set_bar_texture(texture_):
	bar_texture = texture_
	%StatsBar.texture_progress = bar_texture 
	
func set_color(color_):
	color = color_
	%Min.add_theme_color_override("font_color", color_)
	%Max.add_theme_color_override("font_color", color_)
	
