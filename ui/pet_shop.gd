extends GridContainer

var current_spawner: Spawner = null
var current_tile: Tile = null

@export var pet: Sprite2D

@onready var sprite := pet

var name_to_spawner = {} 

func _ready():
	for child in get_children():
		if child is Spawner:
			child.selected.connect(_on_selected)
			child.unselected.connect(_on_unselected)
			name_to_spawner[child.entity_scene.resource_path] = child
	update_spawners.call_deferred()
		
func _process(_delta):
	if current_spawner:
		sprite.global_position = get_viewport().get_mouse_position()
		

func _on_selected(spawner: Spawner):
	current_spawner = spawner
	sprite.texture = spawner.texture
	
func _on_unselected(spawner: Spawner):
	if current_spawner == spawner:
		spawn()
		sprite.texture = null
		current_spawner = null

func spawn():
		if current_tile == null:
			return
	
	
		var entity = current_spawner.entity_scene.instantiate()

		if not entity.get_component(Navigator).movement_types & current_tile.tile_type.movement:
			return
		
			
		entity.position = current_tile.get_center().normalized()
		entity.update_coords() 
		
		entity.add_to_world(get_tree())
		update_spawners()
	
func update_spawners():
	var stage = get_tree().get_first_node_in_group("STAGE")
	if stage:
		for scene_name in stage.max_entities.keys():
			if stage.max_entities[scene_name] > 0:
				name_to_spawner["res://game/creatures/"+scene_name+".tscn"].label.text = "%d" % stage.max_entities[scene_name]
			else:
				name_to_spawner["res://game/creatures/"+scene_name+".tscn"].visible = false
	
