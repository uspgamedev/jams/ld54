@tool
extends Control
class_name Spawner

@export var entity_scene: PackedScene

@export var texture: Texture2D : set = set_texture

signal selected(spawner: Spawner)
signal unselected(spawner: Spawner)

@onready var label = %Label

func set_texture(texture_):
	texture = texture_	
	%TextureRect.texture = texture

func _gui_input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
		if event.is_pressed():
			%TextureRect.texture = null
			selected.emit(self)
		else:
			%TextureRect.texture = texture
			unselected.emit(self)


func _on_texture_rect_mouse_entered():
	var aux_entity = entity_scene.instantiate()
	get_tree().get_first_node_in_group("INSPECTOR").show_entity_desc(aux_entity)
	aux_entity.queue_free()
	
