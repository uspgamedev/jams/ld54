@tool
extends Control

@export_multiline var field_text : String : set = set_field
@export_multiline var value_text : String : set = set_value

@export var double_line := false : set = set_line

var value 

func set_field(field_):
	field_text = field_
	%Field.text = field_
	
func set_value(value_):
	value_text = value_
	%Value.visible = double_line
	%Value2.visible = not double_line
	%Value.text = value_
	%Value2.text = value_

func set_line(line_):
	double_line = line_
	%Value.visible = double_line
	%Value2.visible = not double_line
	update_size()
	if double_line:
		value = %Value
	else:
		value = %Value2

func update_size():
	if double_line:
		custom_minimum_size.y = 50
	else:
		custom_minimum_size.y = 25
	
