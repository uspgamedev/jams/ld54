extends Control

var entity: Entity
var tile: Tile

var spawner := false

func show_entity(entity_: Entity):
	hide_tile()
	entity = entity_
	show_entity_desc(entity_)
	%Display.texture = entity_.get_component(Body).texture
	tile = null
	
func show_tile(tile_: Tile):
	%Bg2.visible = true
	hide_entity()
	tile = tile_
	%Display.texture = tile.get_texture() 
	

func _process(_delta):
	if %Display.texture and not is_instance_valid(entity) and tile == null:
		hide_entity_stats()
	elif is_instance_valid(entity):
		show_entity_stats()
	elif tile:
		show_tile_stats()

func show_entity_stats():
	%Stats.visible = true
	%AnimalStats.visible = true
	
	var fullness = int(100 * entity.get_component(Stats).fed)
	%FedStats.update_bar(fullness, 100)

func hide_entity():		
	hide_entity_stats()
	%AnimalDescriptions.visible = false
	%Display.texture = null
	entity = null
	%Desc.visible = false
	%Name.visible = false
	
func hide_entity_stats():		
	%AnimalStats.visible = false
	%Stats.visible = false
	
func show_tile_stats():
	%Stats.visible = true
	%TileStats.visible = true
	%TileDescriptions.visible = true
	
	%Name.visible = true
	%Name.text =  "[center]" + tile.tile_type.tile_name + "[/center]"
	
	if tile.tile_type.vegetation_max > 0.0:
		var vegetation = int(100 * tile.vegetation )
		%VegetationStats.update_bar(vegetation, int(100*tile.tile_type.vegetation_max))
	%PopulationStats.update_bar(tile.get_entities().size(), tile.tile_type.capacity)
	var terrain = ""
	match tile.tile_type.movement:
		TileType.Movement.LAND:
			terrain = "Land"
		TileType.Movement.WATER:
			terrain = "Water"
		TileType.Movement.AIR:
			terrain = "Air"
			
	%Terrain.value_text = terrain
	
	if not is_zero_approx(tile.tile_type.vegetation_growth_rate):
		%Recover.visible = true
		%Recover.value_text = str(int(1000*tile.tile_type.vegetation_growth_rate)/10.0) + " vegetation/s"
	else:
		%Recover.visible = false
	
func hide_tile():
	%Bg2.visible = false
	%Desc.visible = false
	%Stats.visible = false
	%Display.texture = null
	%TileStats.visible = false
	%TileDescriptions.visible = false
	
	
func show_entity_desc(entity_: Entity):
	hide_tile()
	tile = null
	entity = entity_
	%Name.visible = true
	%Name.text = "[center]" + entity.species_name + "[/center]"
	
	%AnimalStats.visible = false
	%Desc.visible = true
	%AnimalDescriptions.visible = true
	
	%Display.texture = entity.get_component(Body).texture
	
	%ChainDesc.value_text = str(entity.get_component(Creature).prey_level + 1)
	
	var diet = ""
	
	var carnivore = entity.get_component(Carnivore)
	var herbivore = entity.get_component(Herbivore)
	
	if carnivore:
		diet = "Carnivore"
		
	if herbivore:
		diet = "Herbivore"
		
	if carnivore and herbivore:
		diet = "Omnivore"
		
	%DietDesc.value_text = diet
	
	%PreysDesc.visible = carnivore != null
	if carnivore:
		%PreysDesc.value_text = "%s" % carnivore.prey_levels.map(func(value): return str(value + 1)) \
				.reduce(func(string, value): return string + ", " + value)
	
	var movement = ""
	
	var movement_types = entity.get_component(Navigator).movement_types
	
	if movement_types == 0:
		movement = "None"
	elif movement_types == TileType.Movement.LAND + TileType.Movement.WATER + TileType.Movement.AIR:
		movement = "All"
	else:
		var aux = []
		if movement_types & TileType.Movement.LAND:
			aux.push_back("Land")
		if movement_types & TileType.Movement.WATER:
			aux.push_back("Aquatic")
		if movement_types & TileType.Movement.AIR:
			aux.push_back("Aerial")
		movement = aux.reduce(func(string, value): return string + ", " + value)
		
	%LocomotionDesc.value_text = movement
	
	match entity.get_component(Creature).nutrition_level:
		Creature.Nutrition.LOW:
			%NutritionDesc.value_text = "Low"
		Creature.Nutrition.MEDIUM:
			%NutritionDesc.value_text = "Medium"
		Creature.Nutrition.HIGH:
			%NutritionDesc.value_text = "High"

func hide_all():
	hide_tile()
	hide_entity()
