extends Control

@export var remaing_scene : PackedScene

var stage: Stage

var name_to_remaning = {}

func _process(_delta):
	var aux_stage = get_tree().get_first_node_in_group("STAGE")
	
	if aux_stage != stage:
		stage = aux_stage
		for child in %Remaings.get_children():
			%Remaings.remove_child(child)
		add_scenes()
	
	update_reamings()
	
func add_scenes():
	name_to_remaning.clear()
	for condition in stage.victory_condition:
		var scene_path = "res://game/creatures/" + condition.entity_name + ".tscn"
		
		var entity = load(scene_path).instantiate()
		
		var remaning = remaing_scene.instantiate()
		remaning.entity_name = condition.entity_name
		remaning.texture = entity.get_component(Body).texture
		remaning.max_value = condition.entity_number
		
		name_to_remaning[condition.entity_name] = remaning
		
		entity.queue_free()
		%Remaings.add_child(remaning)

func update_reamings():
	for entity_name in stage.entity_numbers.keys():
		if name_to_remaning.has(entity_name):
			name_to_remaning[entity_name].value = stage.entity_numbers[entity_name] 




