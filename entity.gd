extends Node3D
class_name Entity

@export_range(-180.0, 180.0) var longitude = 0.0
@export_range(-90.0, 90.0) var latitude = 0.0

@export var debug: Label3D
@export var species_name: String = ""
@export var entity_name: String = ""

var target_tile: Tile
var target_offset: Vector2 = Vector2.ZERO
var moving := false
@export var speed := 0.2

var tile : Tile

func _ready():
	while true:
		pick_offset()
		await get_tree().create_timer(2.0).timeout

# Called every frame. 'delta' is the elapsed time since the previous frame.

var count = 0

func _process(delta):
#	if count <= 3:
#		count += 1
#		return
#	count = 0
	
	var phi := deg_to_rad(longitude)
	var theta := deg_to_rad(latitude)
	position.z = cos(phi) * cos(theta)
	position.x = sin(phi) * cos(theta)
	position.y = sin(theta)
	
	var camera := get_viewport().get_camera_3d()
	var target: Vector3
	if camera:
		target = camera.global_position
	else:
		target = Vector3.FORWARD
	var direction := (target - position).normalized()
	var up := position.normalized()
	if abs(direction.dot(up)) >= .99:
		up = Vector3.UP
	look_at(target, up)
	
	if not Engine.is_editor_hint() and target_tile:
		var point := target_tile.get_center_with_offset(target_offset)
		var coords: Vector2 = Entity.position_to_coord(point)
		var angular_velocity := get_angular_velocity_to(coords)
		var new_coords: Vector2 = get_coords() + 0.2 * angular_velocity * delta

		var local := transform.basis.inverse() * (target_tile.get_center() - position)
		$Pointer.position.x = local.x
		$Pointer.position.y = local.y
		if new_coords.distance_squared_to(coords) > 0.1:
			moving = true
			longitude = new_coords.x
			latitude = new_coords.y
		else:
			moving = false


func get_coords() -> Vector2:
	return Vector2(longitude, latitude)

func get_angular_velocity_to(target: Vector2) -> Vector2:
	var delta_coords = target - Vector2(longitude, latitude)
	delta_coords.x = deg_to_rad(delta_coords.x)
	delta_coords.y = deg_to_rad(delta_coords.y)

	var lat_speed = speed * sqrt(1.0/ (delta_coords.y**2 + delta_coords.x**2 * sin(latitude)**2)) * delta_coords.y
	var long_speed = speed * sqrt(1.0/ (delta_coords.y**2 + delta_coords.x**2 * sin(latitude)**2)) * delta_coords.x
	
	return Vector2(rad_to_deg(long_speed), rad_to_deg(lat_speed))

var components_cache = {}

func get_component(component_script: Script) -> Node:
	if components_cache.has(component_script):
		return components_cache[component_script] 
		
	for child in get_children():
		if child.get_script() == component_script:
			components_cache[component_script] = child
			return child
	return null

func get_tile() -> Tile:
	return tile

static func get_entity(node: Node) -> Entity:
	var parent = node.get_parent()
	
	while parent and not (parent is Entity):
		parent = parent.get_parent()
	
	return parent as Entity


static func get_world(node: Node) -> World:
	var parent = node.get_parent()
	
	while parent and not (parent is Stage):
		parent = parent.get_parent()
	
	return (parent as Stage).world


static func position_to_coord(p: Vector3) -> Vector2:
	var coord := Vector2()
	coord.x = rad_to_deg(atan2(p.x, p.z))
	coord.y = rad_to_deg(asin(p.y))
	return coord

		
func update_coords():
	var coord := Entity.position_to_coord(position)
	longitude = coord.x
	latitude = coord.y

func add_to_world(scene_tree: SceneTree, reproduction: bool = false, mute := false):
	if not reproduction:
		scene_tree.get_first_node_in_group("STAGE").player_add_entity(self)
	else:
		scene_tree.get_first_node_in_group("ENTITIES").add_child(self)
	await scene_tree.physics_frame
	if mute: return
	if not reproduction:
		$SpawnSFX.play()
	else:
		$ReproductionSFX.play()


func pick_offset():
	target_offset = Vector2(randf(), randf())
