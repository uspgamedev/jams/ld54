extends Node3D

@export var zoom := 1.0 : set = set_zoom 

@onready var camera : Camera3D = $Camera3D

@onready var initial_distance = camera.position.length()

func set_zoom(_zoom):
	if _zoom < 0.05 or _zoom > 2.0 :
		return
	zoom = _zoom
	camera.position = camera.position.normalized() * initial_distance / zoom 
	camera.look_at(Vector3())

func _ready():
	add_to_group("CAMERA")
	$Camera3D/CPUParticles3D.emitting = true

