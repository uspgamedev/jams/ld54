extends Control


@export var speed := 0.005
@export var zoom_speed := 0.03

@onready var camera = get_tree().get_first_node_in_group("CAMERA")


func _unhandled_input(event):
	if Input.is_mouse_button_pressed(MOUSE_BUTTON_RIGHT) and event is InputEventMouseMotion:
		var motion := event as InputEventMouseMotion
		camera.rotation.y -= speed * motion.relative.x / camera.zoom
		camera.rotation.x -= speed * motion.relative.y / camera.zoom
		camera.rotation.x = clampf(camera.rotation.x, -PI/2, PI/2)
		
		
		
	elif event is InputEventMouseButton and event.is_pressed() and \
			(event.button_index == MOUSE_BUTTON_WHEEL_DOWN or event.button_index == MOUSE_BUTTON_WHEEL_UP):
		
		camera.zoom += zoom_speed * (1 if event.button_index == MOUSE_BUTTON_WHEEL_UP else -1) 
	
	

