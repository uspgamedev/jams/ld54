extends Node
class_name Carnivore

@export var prey_levels: Array[int] = [0]
@export var max_fed_to_hunt := 0.7

@onready var _entity := Entity.get_entity(self)

var is_hunting: bool = false

func _physics_process(_delta):
	var stats := _entity.get_component(Stats)
	if _entity.tile and stats.fed <= max_fed_to_hunt:
		var prey: Array[Entity] = []
		for entity in _entity.tile.entities:
			var creature := entity.get_component(Creature) as Creature
			if entity != _entity and creature and creature.prey_level in prey_levels:
				prey.append(entity)
		if not prey.is_empty():
			var victim := prey.pick_random() as Entity
			var creature := victim.get_component(Creature) as Creature
			var nutrition := creature.get_nutrition()
			if not creature.is_queued_for_deletion():
				stats.fed += nutrition
				victim.queue_free()
			$EatingSFX.play()


func _navigate(mask_weights: Dictionary):
	var fed: float = _entity.get_component(Stats).fed
	var weight: float = min(1.0, (1.0 - fed) / (1.0 - max_fed_to_hunt))
	
	if fed <= max_fed_to_hunt:
		weight = 1.0
		if not is_hunting:
			$ChasingSFX.play()
		is_hunting = true
	else:
		is_hunting = false
	
	for prey_level in prey_levels:
		mask_weights[PreyEvaluator.get_mask_name(prey_level)] = weight
