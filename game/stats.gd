extends Node
class_name Stats

@export var hunger_rate := 0.1

@onready var fed := 1.0 : set = set_fed
@onready var death_sound = $DeathSFX

signal hungry()

var hungry_tween : Tween

func _process(delta):
	fed -= hunger_rate * delta
	
func set_fed(_fed):
	fed = max(_fed, 0.0)
	if fed <= 0.0:
		remove_child(death_sound)
		var entity := Entity.get_entity(self)
		entity.add_sibling(death_sound)
		death_sound.play()
		death_sound.finished.connect(func(): death_sound.queue_free())
		kill()

func kill():
	var entity := Entity.get_entity(self)
	entity.queue_free()
	print(entity.name + " died")
	
