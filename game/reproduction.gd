extends Node
class_name Reproduction

@export var min_fed := 0.5
@export var cooldown := 10.0
@onready var child_scene_path : String = Entity.get_entity(self).scene_file_path

@onready var entity := Entity.get_entity(self)
@onready var stats := entity.get_component(Stats)

var cooldown_tween: Tween
var on_cooldown = false

func _ready():
	activate_cooldown()


func _process(_delta):
	if stats.fed >= min_fed and not on_cooldown:
		if entity.tile == null:
#			push_error("Entity doesn't have a tile")
			return
		
		var entities = entity.tile.get_entities()
		
		if entities.size() >= entity.tile.tile_type.capacity:
			return
		
		for partner in entities:
			if partner == entity:
				continue
			var partner_reproduction : Reproduction = partner.get_component(Reproduction)
			if partner_reproduction and \
					partner_reproduction.child_scene_path == child_scene_path and \
					not partner_reproduction.on_cooldown:
				
				partner_reproduction.activate_cooldown()
				self.activate_cooldown()
				
				var child = load(child_scene_path).instantiate()
				
				child.position = entity.position
				child.update_coords()
				
				child.add_to_world(get_tree(), true)
				child.get_component(Reproduction).activate_cooldown()
				print(child.name + " was born")


#func _navigate(mask_weights: Dictionary):
#	mask_weights[CapacityEvaluator.MASK] = -.5
				
				
		
func activate_cooldown():
	on_cooldown = true
	cooldown_tween = create_tween()
	cooldown_tween.tween_interval(cooldown)
	cooldown_tween.finished.connect(func(): 
		cooldown_tween = null
		on_cooldown = false
	)
				
