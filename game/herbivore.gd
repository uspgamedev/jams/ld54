extends Node
class_name Herbivore

@export var eating_speed := 0.5

@onready var _entity = Entity.get_entity(self)

var hungry := false

var count = 0

func _physics_process(delta):
	if count % 5 == 0:
		try_eat(delta)
	count += 1


func _navigate(mask_weights: Dictionary):
	var fed := _entity.get_component(Stats).fed as float
	var weight: float = min(1.0, (1.0 - fed) / 0.5)
	mask_weights[VegetationEvaluator.MASK] = weight


func try_eat(delta):
	var stats := _entity.get_component(Stats)
	if _entity.tile and _entity.tile.vegetation >= 0.0 and stats.fed < 1.0:
		var amount: float = min(
			_entity.tile.vegetation,
			_entity.tile.vegetation * eating_speed * delta
		)
		_entity.tile.vegetation -= amount
		_entity.get_component(Stats).fed += amount
		
