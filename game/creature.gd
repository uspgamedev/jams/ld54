extends Node
class_name Creature

@export var prey_level := 0
@export var nutrition_level := Nutrition.LOW

enum Nutrition {LOW, MEDIUM, HIGH}


func get_nutrition() -> float:
	match nutrition_level:
		Nutrition.LOW:
			return 0.15
		Nutrition.MEDIUM:
			return 0.5
		Nutrition.HIGH:
			return 0.75
	
	return 0.0
