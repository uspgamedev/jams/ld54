class_name Stage
extends Node3D

signal stage_won
signal stage_lost
signal stage_reset

@export var world_scn: PackedScene
@export var victory_condition: Array[VictoryCondition]
@export var max_entities: Dictionary = {
	"bat": 0,
	"bird": 0,
	"dogguru": 0,
	"eel": 0,
	"fairy": 0,
	"fish": 0,
	"frogish": 0,
	"hippo": 0,
	"hitode": 0,
	"lizard": 0,
	"parrotsaurus": 0,
	"sheep": 0,
	"tirge": 0,
	"whale": 0,
	"worm": 0 }
@export var time_to_lose: float   

var entity_numbers: Dictionary = {
	"bat": 0,
	"bird": 0,
	"dogguru": 0,
	"eel": 0,
	"fairy": 0,
	"fish": 0,
	"frogish": 0,
	"hippo": 0,
	"hitode": 0,
	"lizard": 0,
	"parrotsaurus": 0,
	"sheep": 0,
	"tirge": 0,
	"whale": 0,
	"worm": 0 }
var lose_condition: Tween
var world: World

func _ready():
	max_entities = max_entities.duplicate()
	add_to_group("STAGE")
	world = world_scn.instantiate()
	add_child(world)
	lose_condition = create_tween()
	lose_condition.tween_interval(time_to_lose)
	lose_condition.finished.connect(loss)


func _on_entities_child_entered_tree(node):
	if not node is Entity:
		return
	var entity_name = Stage.extract_entity_name(node.scene_file_path)
	if entity_numbers.has(entity_name):
		entity_numbers[entity_name] += 1
		check_victory_condition()


func player_add_entity(node):
	var entity_name = Stage.extract_entity_name(node.scene_file_path)
	if entity_numbers.has(entity_name):
		if max_entities[entity_name] > 0:
			$Entities.add_child(node)
			node.tree_exited.connect(func(): entity_numbers[entity_name] -= 1)
		if max_entities[entity_name] > 0:
			max_entities[entity_name] -= 1
	else:
		$Entities.add_child(node)


static func extract_entity_name(path: String):
	var entity_name = path.get_file()
	entity_name = entity_name.get_slice(".", 0)
	return entity_name


func check_victory_condition():
	for entity in victory_condition:
		if entity_numbers[entity.entity_name] < entity.entity_number:
			return
	victory()


func victory():
#	get_tree().paused = true
#	$VictorySound.play()
#	await $VictorySound.finished
#	get_tree().paused = false
	stage_won.emit()


func _on_ui_stage_reset():
	stage_reset.emit()


func loss():
	# I II 
	# II I_
#	get_tree().paused = true
#	$LossSound.play()
#	await $LossSound.finished
#	get_tree().paused = false
	stage_lost.emit()
