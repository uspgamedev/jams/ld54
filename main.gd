class_name Main
extends Node3D


@export var stages: Array[PackedScene] = []

@onready var _current_index := 0
@onready var _win_label := $"%WinLabel"

var _stage: Stage
var _started := false


func _physics_process(_delta):
	if not _stage and _started:
		if _current_index >= stages.size():
			_win_label.show()
			return
		print("Starting stage %d" % (_current_index + 1))
		_stage = stages[_current_index].instantiate() as Stage
		_stage.stage_won.connect(self._on_stage_won)
		_stage.stage_lost.connect(self._on_stage_lost)
		_stage.stage_reset.connect(self._on_stage_reset)
		add_child(_stage)


func _on_stage_won():
	%WonScreen.visible = true
	_stage.queue_free()
	_current_index += 1
	%VictorySound.play()
	create_tween().tween_interval(5).finished.connect(
		func():
			%WonScreen.visible = false
	)
	


func _on_stage_lost():
	%LossSound.play()
	create_tween().tween_interval(5).finished.connect(
		func():
			_stage.queue_free()
	)


func _on_stage_reset():
	_stage.queue_free()


func _on_play_button_pressed():
	_started = true
	$"%Start".hide()
