@tool
class_name World
extends Node3D

@export var generate := false : set = set_aux
@export var tile_scn: PackedScene
@export var tiles: Node3D
@export var tile_graph: TileGraph

@onready var sphere := $Icosphere as MeshInstance3D


func set_aux(_aux):
	_generate()


func get_neighbors(tile: Tile) -> Array[Tile]:
	if not tile_graph:
		return []
	var neighbors: Array[Tile] = []
	for neighbor in tile_graph.neighbors[get_path_to(tile)]:
		neighbors.append(get_node(neighbor) as Tile)
	return neighbors


func _generate():
	var graph_builder := TileGraphBuilder.new(self)
	var points := sphere.mesh.get_faces()

	for tile in tiles.get_children():
		tile.free()
	
	for i in points.size()/3:
		var vertices = PackedVector3Array([
			points[3*i],
			points[3*i+1],
			points[3*i+2],
		])
		var uv = Tile.get_uvs()[0]
		
		var mesh = ArrayMesh.new()
		var arrays = []
		arrays.resize(Mesh.ARRAY_MAX)
		arrays[Mesh.ARRAY_VERTEX] = vertices
		arrays[Mesh.ARRAY_TEX_UV] = uv

		mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
		
		var tile := tile_scn.instantiate() as MeshInstance3D
		tile.mesh = mesh
		tile.collision_shape = mesh.create_trimesh_shape()
	
		tiles.add_child(tile)
		tile.set_owner(get_tree().edited_scene_root)
		graph_builder.add_tile(tile, vertices)
	#graph_builder.dump()
	tile_graph = graph_builder.build()
		

